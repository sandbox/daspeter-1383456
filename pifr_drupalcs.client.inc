<?php

/**
 * @file
 *   Provide drupalcs client review implementation.
 */

module_load_include('client.inc', 'pifr_drupal');

/**
 * drupalcs client review implementation.
 */
class pifr_client_review_pifr_drupalcs extends pifr_client_review_pifr_drupal {

  /**
   * Maximum run time for drupalcs review.
   *
   * @var integer
   */
  protected $max_run_time = 600;

  /**
   * Code results collected during review().
   *
   * @var array
   */
  protected $results = array();

  public function __construct(array $test) {
    parent::__construct($test);

    variable_set('pifr_drupalcs_status', array(
      'position' => 0,
      'total' => 0,
    ));
  }

  /**
   * Perform drupalcs review.
   */
  protected function review() {
    // Since the drupalcs reviews are run inside this script instead of externally
    // the script need to have enough time to finish.
    set_time_limit($this->max_run_time);

    // Load PEAR PHP_CodeSniffer.
    require_once variable_get('pifr_drupalcs_pear_location', NULL) . 'CodeSniffer.php';
    $phpcs = new PHP_CodeSniffer_CLI();
    $phpcs->checkRequirements();

    // Prepare the base arguments array.
    $values = array(
      'standard' => variable_get('pifr_drupalcs_standard', 'DrupalCodingStandard'),
      'report' => 'xml',
      'asp_tags' => FALSE,
      'files' => array(),
    );

    // @todo Should we better pass the whole files array instead every single
    // one?
    // Cycle through all relevant files and perform review.
    $files = $this->syntax_ignore($this->file_list());
    // Keep track of status for use by status().
    $status = variable_get('pifr_drupalcs_status', array(
      'position' => 0,
      'total' => 0,
    ));
    $status['total'] = count($files);

    $not_found = 0;
    foreach ($files as $file) {
      // Update variable status to keep status() up-to-date.
      $status['position']++;
      variable_set('pifr_drupalcs_status', $status);

      $filepath = $this->checkout_directory . '/' . $file;

      // Ensure that the file exists and is not a reference to a file that was
      // removed in a patch, or the like.
      if (!file_exists($filepath)) {
        $not_found++;
        continue;
      }

      $values['files'] = array($filepath);
      $this->results[$file] = $phpcs->process($values);
    }

    $this->log('Reviewed [' . (count($files) - $not_found) . ' of ' . count($files) . '] relevant file(s).');
  }

  /**
   * Get a list of files that should be reviewed by drupalcs.
   *
   * @return array List of files that should be reviewed by drupalcs.
   */
  protected function file_list() {
    if (!empty($this->test['files'])) {
      return $this->syntax_files();
    }
    elseif (!empty($this->arguments['test.files'])) {
      return $this->syntax_files();
    }
    elseif (!empty($this->arguments['drupal.modules'])) {
      return $this->syntax_files();
    }
    elseif (!empty($this->arguments['test.directory.review'][0])) {
      return $this->syntax_files();
    }
    elseif (PIFR_SHORTCUT_CORE_TEST) {
      return array('index.php');
    }
    return $this->syntax_files();
  }

  /**
   * Add the drupalcs results.
   */
  public function get_result() {
    $result = parent::get_result();

    // If result code is fail then all the operations passed, so the test
    // results need to be added and the proper result code assigned.
    if ($result['code'] == $this->result_codes['fail']) {
      $result['details'] = array(
        '@pass' => 0,
        '@fail' => 0,
        '@exception' => 0,
      );
      $result['data'] = array();

      // Cycle through results for each file.
      $count = 0;
      foreach ($this->results as $file => $review) {
        $result['data'][$file] = array(
          'test_name' => $file,
          'pass' => 0,
          'fail' => 0,
          'exception' => 0,
          'assertions' => array(),
        );

        $review = simplexml_load_string($review);

        $result['data'][$file]['fail'] = count($review->file->error);
        $result['data'][$file]['pass'] = count($review->file->warning);

        // Summarize total
        $result['details']['@fail'] += $result['data'][$file]['fail'];
        $result['details']['@pass'] += $result['data'][$file]['pass'];


        $assertion_types = array('fail' => 'error' , 'pass' => 'warning');
        foreach ($assertion_types as $assertion_name => $review_container) {
          if ($result['data'][$file][$assertion_name]) {
            foreach ($review->file->{$review_container} as $xml_output) {
              // Store non-pass assertion until reaching maximum number.
              if ($count < PIFR_CLIENT_MAX_ASSERTIONS) {
                // Make file only include name, instead of absolute reference.
                $assertion['file'] = basename($file);
                $assertion['line'] = (string) $xml_output['line'];
                $assertion['message'] = $review_container . ': ' . (string) $xml_output;

                // Add assertion to set of non-pass assertions for the class.
                $result['data'][$file]['assertions'][] = $assertion;
                // Keep a record of the number of assertions stored.
                $count++;
              }
            }
          }
        }
      }

      // Remove file name keys.
      $result['data'] = array_values($result['data']);

      // Assign the code bassed on the test results.
      $result['code'] = $this->result_codes[$result['details']['@fail'] + $result['details']['@exception'] ? 'fail' : 'pass'];
    }
    return $result;
  }

  /**
   * If on review operation then display the drupalcs review position.
   */
  public static function status() {
    if (variable_get('pifr_client_operation', 'init') == 'review') {
      $status = variable_get('pifr_drupalcs_status', array(
        'position' => 0,
        'total' => 0,
      ));

      return t('Reviewing file @position of @total.', array(
        '@position' => number_format($status['position']),
        '@total' => number_format($status['total']),
      ));
    }
    return parent::status();
  }
}
