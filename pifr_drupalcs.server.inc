<?php

/**
 * @file
 *   Provide drupalcs server review implementation.
 */

module_load_include('server.inc', 'pifr_drupal');

/**
 * drupalcs server review implementation.
 */
class pifr_server_review_pifr_drupalcs extends pifr_server_review_pifr_drupal {

  protected $labels = array(
    'pass' => 'minor',
    'fail' => 'critical',
    'exception' => 'normal',
  );

  protected $detail_message = '@pass minor(s), @fail critical(s), and @exception normal(s)';

  /**
   * Modify step information and append default arguments.
   */
  public function __construct() {
    parent::__construct();

    unset($this->steps['install']);
    $this->steps['review'] = array_merge($this->steps['review'], array(
      'confirmation' => FALSE,
    ));
    $this->steps['fail'] = array_merge($this->steps['fail'], array(
      'title' => 'drupalcs flags',
      'active title' => 'detect drupalcs flags',
      'description' => 'Ensure that your code follows the Drupal standard and passes a drupalcs & tough love review.',
      'confirmation' => 'pifr_drupalcs',
    ));
    $this->steps['pass'] = array_merge($this->steps['pass'], array(
      'title' => 'drupalcs review passed',
      'active title' => 'complete drupalcs review with all passed',
      'summary' => '@pass minor(s)',
      'confirmation' => 'pifr_drupalcs',
    ));
  }
}
